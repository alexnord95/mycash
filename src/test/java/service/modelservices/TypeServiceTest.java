package service.modelservices;

import mycash.db.dao.TypeDao;
import mycash.db.entities.Type;
import mycash.service.modelservices.TypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import mycash.service.convert.ConvertService;
import mycash.service.dto.TypeDto;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TypeServiceTest {
    private static Type type = new Type();
    private static TypeDto convType = new TypeDto(
            type.getTypeId(),
            type.getName());

    static {
        type.setTypeId(1);
        type.setName("accType");
    }

    @InjectMocks
    TypeService subj;
    @Mock
    TypeDao typeDaoMock;
    @Mock
    ConvertService convertServiceMock;

    @Test
    public void viewAllTypes_typesNotFound() {
        when(typeDaoMock.findByAll()).thenReturn(Collections.EMPTY_LIST);

        List<TypeDto> typeDtos = subj.viewAllTypes();
        assertEquals(Collections.EMPTY_LIST, typeDtos);
    }

    @Test
    public void viewAllTypes_success() {
        List<Type> types = asList(type, type);
        List<TypeDto> convTypes = asList(convType, convType);
        when(typeDaoMock.findByAll()).thenReturn(types);
        when(convertServiceMock.convert(type)).thenReturn(convType);

        List<TypeDto> typeDtos = subj.viewAllTypes();
        assertEquals(convTypes, typeDtos);
    }
}