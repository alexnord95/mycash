package service.modelservices;

import mycash.db.dao.CategoryDao;
import mycash.db.entities.Category;
import mycash.service.modelservices.CategoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import mycash.service.convert.ConvertService;
import mycash.service.create.CreateService;
import mycash.service.dto.CategoryDto;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {
    private static String name = "category";
    private static Category category = new Category();
    private static CategoryDto convCategory = new CategoryDto(
            category.getCategoryId(),
            category.getName());

    static {
        category.setCategoryId(1);
        category.setName(name);
    }

    @InjectMocks
    CategoryService subj;
    @Mock
    CategoryDao categoryDaoMock;
    @Mock
    ConvertService convertServiceMock;
    @Mock
    CreateService createServiceMock;

    @Test
    public void viewAllCategories_categoriesNotFound() {
        when(categoryDaoMock.findByAll()).thenReturn(null);

        List<CategoryDto> categoryDtos = subj.viewAllCategories();
        assertNull(categoryDtos);
    }

    @Test
    public void viewAllCategories_success() {
        List<Category> categories = asList(category, category);
        List<CategoryDto> convCategories = asList(convCategory, convCategory);
        when(categoryDaoMock.findByAll()).thenReturn(categories);
        when(convertServiceMock.convert(category)).thenReturn(convCategory);

        List<CategoryDto> categoryDtos = subj.viewAllCategories();
        assertEquals(convCategories, categoryDtos);
    }

    @Test
    public void registerNewCategory_categoryNotCreated() {
        when(createServiceMock.createNewCategory(name)).thenReturn(null);

        CategoryDto newCategory = subj.registerNewCategory(name);
        assertNull(newCategory);
    }

    @Test
    public void registerNewCategory_success() {
        when(createServiceMock.createNewCategory(name)).thenReturn(category);
        when(convertServiceMock.convert(category)).thenReturn(convCategory);

        CategoryDto newCategory = subj.registerNewCategory(name);
        assertEquals(convCategory, newCategory);
    }
}