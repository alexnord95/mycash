package service.modelservices;

import mycash.db.dao.AccountDao;
import mycash.db.dao.TransactionDao;
import mycash.db.dao.UserDao;
import mycash.db.entities.*;
import mycash.service.convert.ConvertService;
import mycash.service.create.CreateService;
import mycash.service.dto.*;
import mycash.service.modelservices.AccountService;
import mycash.service.modelservices.CategoryService;
import mycash.service.modelservices.TransactionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static java.sql.Date.valueOf;
import static java.time.LocalDate.now;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {
    private static Integer accNum = 1111;
    private static Integer userId = 1;
    private static BigDecimal amount = BigDecimal.valueOf(100);
    private static String operation = "transfer";
    private static String categoryId = "1 3";
    private static User user = new User();
    private static UserDto convUser = new UserDto();
    private static Type accType = new Type();
    private static TypeDto convType = new TypeDto();
    private static Account account = new Account();
    private static AccountDto convAcc = new AccountDto();
    private static Transaction transaction = new Transaction();
    private static TransactionDto convTransaction = new TransactionDto();
    private static Category category = new Category();
    private static List<Category> categories = asList(category, category);
    private static CategoryDto convCategory = new CategoryDto();

    static {
        user
                .setUserId(userId)
                .setName("name")
                .setEmail("name@mail.ru")
                .setPassword("pass")
                .setActive(true);

        convUser
                .setUserId(user.getUserId())
                .setName(user.getName())
                .setEmail(user.getEmail())
                .setActive(user.isActive());

        accType
                .setTypeId(1)
                .setName("type");

        convType
                .setTypeId(accType.getTypeId())
                .setName(accType.getName());

        account
                .setAccountId(1)
                .setUser(user)
                .setTitle("source")
                .setBalance(BigDecimal.valueOf(500))
                .setType(accType)
                .setAccountNumber(accNum);

        convAcc
                .setAccountId(account.getAccountId())
                .setUser(convUser)
                .setTitle(account.getTitle())
                .setBalance(account.getBalance())
                .setType(convType)
                .setAccountNumber(account.getAccountNumber());


        transaction
                .setTransactionId(1)
                .setAccountSource(account)
                .setAccountDestination(account)
                .setOperation(operation)
                .setAmount(amount)
                .setDate(valueOf(now()));

        convTransaction
                .setTransactionId(transaction.getTransactionId())
                .setAccountSource(convAcc)
                .setAccountDestination(convAcc)
                .setOperation(transaction.getOperation())
                .setAmount(transaction.getAmount())
                .setDate(transaction.getDate());

        category
                .setCategoryId(1)
                .setName("category");

        convCategory
                .setCategoryId(category.getCategoryId())
                .setName(category.getName());

    }

    @InjectMocks
    TransactionService subj;
    @Mock
    TransactionDao transactionDaoMock;
    @Mock
    AccountDao accountDaoMock;
    @Mock
    UserDao userDaoMock;
    @Mock
    ConvertService convertServiceMock;
    @Mock
    CreateService createServiceMock;
    @Mock
    AccountService accountServiceMock;
    @Mock
    CategoryService categoryServiceMock;

    @Test
    public void viewAllTransactions_accountNotFound() {
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(accountDaoMock.findByNumberAndUser(accNum, user)).thenReturn(null);
        List<TransactionDto> transactionDtos = subj.viewAllTransactions(accNum, userId);
        assertEquals(Collections.EMPTY_LIST, transactionDtos);
    }

    @Test
    public void viewAllTransactions_transactionsNotExist() {
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(accountDaoMock.findByNumberAndUser(accNum, user)).thenReturn(account);
        when(transactionDaoMock.findByAcc(account)).thenReturn(Collections.EMPTY_LIST);

        List<TransactionDto> transactionDtos = subj.viewAllTransactions(accNum, userId);
        assertEquals(Collections.EMPTY_LIST, transactionDtos);
    }

    @Test
    public void viewAllTransactions_success() {
        List<Transaction> transactions = asList(transaction, transaction);
        List<TransactionDto> convTransactions = asList(convTransaction, convTransaction);
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(accountDaoMock.findByNumberAndUser(accNum, user)).thenReturn(account);
        when(transactionDaoMock.findByAcc(account)).thenReturn(transactions);
        when(convertServiceMock.convert(transaction)).thenReturn(convTransaction);

        List<TransactionDto> transactionDtos = subj.viewAllTransactions(accNum, userId);
        assertEquals(convTransactions, transactionDtos);
    }

    @Test
    public void registerNewTransaction_accountsNotFound() {
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(accountDaoMock.findByNumberAndUser(accNum, user)).thenReturn(null);
        when(accountDaoMock.findByNumber(accNum)).thenReturn(null);
        TransactionDto transactionDto = subj.registerNewTransaction(accNum, accNum, operation, amount, categoryId, userId);
        assertNull(transactionDto);
    }

    @Test
    public void registerNewTransaction_transactionNotPassed() {
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(accountDaoMock.findByNumberAndUser(accNum, user)).thenReturn(account);
        when(accountDaoMock.findByNumber(accNum)).thenReturn(account);
        when(categoryServiceMock.findCategoriesById(categoryId)).thenReturn(categories);
        when(createServiceMock.createNewTransaction(account, account, operation, amount, categories)).thenReturn(null);

        TransactionDto transactionDto = subj.registerNewTransaction(accNum, accNum, operation, amount, categoryId, userId);
        assertNull(transactionDto);
    }

    @Test
    public void registerNewTransaction_success() {
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(accountDaoMock.findByNumberAndUser(accNum, user)).thenReturn(account);
        when(accountDaoMock.findByNumber(accNum)).thenReturn(account);
        when(categoryServiceMock.findCategoriesById(categoryId)).thenReturn(categories);
        when(createServiceMock.createNewTransaction(account, account, operation, amount, categories)).thenReturn(transaction);
        when(convertServiceMock.convert(transaction)).thenReturn(convTransaction);

        TransactionDto newTransaction = subj.registerNewTransaction(accNum, accNum, operation, amount, categoryId, userId);
        assertEquals(convTransaction, newTransaction);

    }
}