package service.modelservices;

import mycash.db.dao.AccountDao;
import mycash.db.dao.TypeDao;
import mycash.db.dao.UserDao;
import mycash.db.entities.Account;
import mycash.db.entities.Type;
import mycash.db.entities.User;
import mycash.service.modelservices.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import mycash.service.convert.ConvertService;
import mycash.service.create.CreateService;
import mycash.service.dto.AccountDto;
import mycash.service.dto.TypeDto;
import mycash.service.dto.UserDto;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    private static Integer userId = 1;
    private static Integer accTypeId = 2;
    private static String title = "title";
    private static Type accType = new Type();
    private static TypeDto convType = new TypeDto();
    private static User user = new User();
    private static UserDto convUser = new UserDto();
    private static Account account = new Account();
    private static AccountDto convAcc = new AccountDto();

    static {
        user.setUserId(1)
                .setName("name")
                .setEmail("name@mail.ru")
                .setPassword("pass")
                .setActive(true);

        convUser.setUserId(user.getUserId())
                .setName(user.getName())
                .setEmail(user.getEmail())
                .setActive(user.isActive());

        accType.setTypeId(accTypeId)
                .setName("type");

        convType.setTypeId(accType.getTypeId())
                .setName(accType.getName());

        account.setAccountId(1)
                .setUser(user)
                .setTitle("source")
                .setBalance(BigDecimal.valueOf(500))
                .setType(accType)
                .setAccountNumber(1111);

        convAcc.setAccountId(account.getAccountId())
                .setUser(convUser)
                .setTitle(account.getTitle())
                .setBalance(account.getBalance())
                .setType(convType)
                .setAccountNumber(account.getAccountNumber());
    }

    @InjectMocks
    AccountService subj;
    @Mock
    UserDao userDaoMock;
    @Mock
    AccountDao accountDaoMock;
    @Mock
    TypeDao typeDaoMock;
    @Mock
    ConvertService convertServiceMock;
    @Mock
    CreateService createServiceMock;

    @Test
    public void viewAllAccByUserId_userNotFound() {
        when(userDaoMock.findById(userId)).thenReturn(null);

        List<AccountDto> accountDtos = subj.viewAllAccByUserId(userId);
        assertEquals(Collections.EMPTY_LIST, accountDtos);
    }

    @Test
    public void viewAllAccByUserId_accountsNotExist() {
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(accountDaoMock.findByUser(user)).thenReturn(Collections.EMPTY_LIST);

        List<AccountDto> accountDtos = subj.viewAllAccByUserId(userId);
        assertEquals(Collections.EMPTY_LIST, accountDtos);
    }

    @Test
    public void viewAllAccByUserId_success() {
        List<Account> accounts = asList(account, account);
        List<AccountDto> convAccs = asList(convAcc, convAcc);

        when(userDaoMock.findById(userId)).thenReturn(user);
        when(accountDaoMock.findByUser(user)).thenReturn(accounts);
        when(convertServiceMock.convert(account)).thenReturn(convAcc);

        List<AccountDto> accountDtos = subj.viewAllAccByUserId(userId);
        assertEquals(convAccs, accountDtos);
    }

    @Test
    public void registerNewAccount_userNotFound() {
        when(userDaoMock.findById(userId)).thenReturn(null);

        AccountDto newAccount = subj.registerNewAccount(userId, title, accTypeId);
        assertNull(newAccount);
    }

    @Test
    public void registerNewAccount_typeNotFound() {
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(typeDaoMock.findById(accTypeId)).thenReturn(null);

        AccountDto newAccount = subj.registerNewAccount(userId, title, accTypeId);
        assertNull(newAccount);
    }

    @Test
    public void registerNewAccount_success() {
        when(userDaoMock.findById(userId)).thenReturn(user);
        when(typeDaoMock.findById(accTypeId)).thenReturn(accType);
        when(createServiceMock.createNewAccount(user, title, accType)).thenReturn(account);
        when(convertServiceMock.convert(account)).thenReturn(convAcc);

        AccountDto newAccount = subj.registerNewAccount(userId, title, accTypeId);
        assertEquals(convAcc, newAccount);
    }
}