package service.modelservices;

import mycash.db.dao.UserDao;
import mycash.db.entities.User;
import mycash.service.modelservices.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import mycash.service.convert.ConvertService;
import mycash.service.create.CreateService;
import mycash.service.dto.UserDto;
import mycash.service.hash.HashService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    private static String name = "Alex";
    private static String email = "alex@mail.ru";
    private static String pass = "password";
    private static String passHash = "password_hash";
    private static User user = new User();

    static {
        user.setUserId(1);
        user.setName(name);
        user.setEmail("alex@mail.ru");
        user.setPassword(pass);
        user.setActive(true);
    }

    @InjectMocks
    UserService subj;
    @Mock
    UserDao userDaoMock;
    @Mock
    HashService hashServiceMock;
    @Mock
    ConvertService convertServiceMock;
    @Mock
    CreateService createServiceMock;

    @Test
    public void auth_userNotFound() {
        when(hashServiceMock.hash(pass)).thenReturn(passHash);
        when(userDaoMock.findByNameAndPass(name, passHash)).thenReturn(null);

        UserDto user = subj.auth(name, pass);

        assertNull(user);
    }

    @Test
    public void auth_success() {
        UserDto convUser = new UserDto(
                user.getUserId(),
                user.getName(),
                user.getEmail(),
                user.isActive());

        when(hashServiceMock.hash(pass)).thenReturn(passHash);
        when(userDaoMock.findByNameAndPass(name, passHash)).thenReturn(user);
        when(convertServiceMock.convert(user)).thenReturn(convUser);

        UserDto userDto = subj.auth(name, pass);

        assertEquals(userDto.getUserId(), convUser.getUserId());
        assertEquals(userDto.getName(), convUser.getName());
        assertEquals(userDto.getEmail(), convUser.getEmail());
        assertEquals(userDto.isActive(), convUser.isActive());
    }

    @Test
    public void registration_userNotCreated() {
        when(createServiceMock.createNewUser(name, email, pass)).thenReturn(null);

        UserDto userDto = subj.registerNewUser(name, email, pass);

        assertNull(userDto);
    }

    @Test
    public void registration_success() {
        UserDto convUser = new UserDto(
                user.getUserId(),
                user.getName(),
                user.getEmail(),
                user.isActive());

        when(createServiceMock.createNewUser(name, email, pass)).thenReturn(user);
        when(convertServiceMock.convert(user)).thenReturn(convUser);

        UserDto newUser = subj.registerNewUser(name, email, pass);

        assertEquals(convUser, newUser);
    }
}