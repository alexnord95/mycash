package mycash.webview.controller.transaction;

import lombok.Data;

@Data
public class ViewAccTransactionsRequest {
    private Integer accNumber;
}
