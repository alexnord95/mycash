package mycash.webview.controller.transaction;

import mycash.service.dto.TransactionDto;
import mycash.service.dto.UserDto;
import mycash.service.modelservices.TransactionService;
import mycash.webview.controller.Controller;
import mycash.webview.servlet.AuthUserServlet;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service(value = "/authed/view-acc-transactions")
public class ViewAccTransactionsController extends Controller<ViewAccTransactionsRequest, ViewAccTransactionsResponse> {
    public ViewAccTransactionsController() {
        super.setHttpMethodName("POST");
    }

    @Override
    public ViewAccTransactionsResponse handle(ViewAccTransactionsRequest request, ApplicationContext context) {
        ViewAccTransactionsResponse response = new ViewAccTransactionsResponse();
        UserDto user = (UserDto) AuthUserServlet.getSession().getAttribute("user");
        List<TransactionDto> transactions = context.getBean(TransactionService.class).viewAllTransactions(
                request.getAccNumber(), user.getUserId());
        if (transactions.size() == 0) {
            response.setMessage("Transactions not found")
                    .setTransactions(Collections.EMPTY_LIST);
        } else {
            response.setMessage("Account transactions")
                    .setTransactions(transactions);
        }
        return response;
    }

    @Override
    public Class<ViewAccTransactionsRequest> getRequestClass() {
        return ViewAccTransactionsRequest.class;
    }
}
