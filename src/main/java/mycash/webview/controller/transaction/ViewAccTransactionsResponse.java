package mycash.webview.controller.transaction;

import lombok.Data;
import lombok.experimental.Accessors;
import mycash.service.dto.TransactionDto;

import java.util.List;

@Data
@Accessors(chain = true)
public class ViewAccTransactionsResponse {
    private String message;
    private List<TransactionDto> transactions;
}
