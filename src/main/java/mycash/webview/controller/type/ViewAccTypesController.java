package mycash.webview.controller.type;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import mycash.service.dto.TypeDto;
import mycash.service.modelservices.TypeService;
import mycash.webview.controller.Controller;

import java.util.List;

@Service(value = "/authed/view-acc-types")
public class ViewAccTypesController extends Controller<ViewAccTypesRequest, ViewAccTypesResponse> {
    public ViewAccTypesController() {
        super.setHttpMethodName("GET");
    }

    @Override
    public ViewAccTypesResponse handle(ApplicationContext context) {
        ViewAccTypesResponse response = new ViewAccTypesResponse();
        List<TypeDto> types = context.getBean(TypeService.class)
                .viewAllTypes();
        response.setMessage("Account types")
                .setTypes(types);
        return response;
    }

    @Override
    public Class<ViewAccTypesRequest> getRequestClass() {
        return ViewAccTypesRequest.class;
    }
}
