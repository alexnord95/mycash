package mycash.webview.controller.type;

import lombok.Data;
import lombok.experimental.Accessors;
import mycash.service.dto.TypeDto;

import java.util.List;

@Data
@Accessors(chain = true)
public class ViewAccTypesResponse {
    private String message;
    private List<TypeDto> types;
}
