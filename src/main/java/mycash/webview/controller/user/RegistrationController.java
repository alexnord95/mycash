package mycash.webview.controller.user;

import mycash.webview.controller.Controller;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import mycash.service.dto.UserDto;
import mycash.service.modelservices.UserService;

@Service(value = "/auth/registration")
public class RegistrationController extends Controller<RegistrationRequest, RegistrationResponse> {

    public RegistrationController() {
        super.setHttpMethodName("POST");
    }

    @Override
    public RegistrationResponse handle(RegistrationRequest request, ApplicationContext context) {
        RegistrationResponse response = new RegistrationResponse();
        UserDto user = context.getBean(UserService.class).registerNewUser(
                request.getName().toLowerCase(),
                request.getEmail().toLowerCase(),
                request.getPassword());
        if (user != null) {
            response.setMessage("User registered");
            response.setUser(user);
        } else {
            response.setMessage("User registration failed");
            response.setUser(new UserDto());
        }

        return response;
    }

    @Override
    public Class<RegistrationRequest> getRequestClass() {
        return RegistrationRequest.class;
    }
}
