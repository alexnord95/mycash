package mycash.webview.controller.user;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import mycash.service.dto.UserDto;
import mycash.service.modelservices.UserService;
import mycash.webview.controller.Controller;
import mycash.webview.servlet.AuthUserServlet;

@Service(value = "/auth/authorization")
public class AuthController extends Controller<AuthRequest, AuthResponse> {

    public AuthController() {
        super.setHttpMethodName("POST");
    }

    @Override
    public AuthResponse handle(AuthRequest request, ApplicationContext context) {
        AuthResponse response = new AuthResponse();
        UserDto user = context.getBean(UserService.class).auth(
                request.getName().toLowerCase(),
                request.getPassword());
        if (user != null) {
            AuthUserServlet.getSession().setAttribute("user", user);
            response.setMessage("Hello, " + user.getName());
        } else {
            response.setMessage("Wrong username or password");
        }
        return response;
    }

    @Override
    public AuthResponse handle(ApplicationContext context) {
        return null;
    }

    @Override
    public Class<AuthRequest> getRequestClass() {
        return AuthRequest.class;
    }
}
