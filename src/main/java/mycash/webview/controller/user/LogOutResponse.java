package mycash.webview.controller.user;

import lombok.Data;

@Data
public class LogOutResponse {
    private String message;
}
