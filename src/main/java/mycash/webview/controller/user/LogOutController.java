package mycash.webview.controller.user;

import mycash.webview.controller.Controller;
import mycash.webview.servlet.AuthUserServlet;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import mycash.service.dto.UserDto;

@Service(value = "/authed/logout")
public class LogOutController extends Controller<LogOutRequest, LogOutResponse> {
    public LogOutController() {
        super.setHttpMethodName("GET");
    }

    @Override
    public LogOutResponse handle(ApplicationContext context) {
        LogOutResponse response = new LogOutResponse();
        UserDto user = (UserDto) AuthUserServlet.getSession().getAttribute("user");
        response.setMessage("Sign out: " + user.getName());
        AuthUserServlet.setSession(null);
        return response;
    }

    @Override
    public Class<LogOutRequest> getRequestClass() {
        return LogOutRequest.class;
    }
}
