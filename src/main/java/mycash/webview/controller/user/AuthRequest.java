package mycash.webview.controller.user;

import lombok.Data;

@Data
public class AuthRequest {
    private String name;
    private String password;
}
