package mycash.webview.controller.user;

import lombok.Data;
import mycash.service.dto.UserDto;

@Data
public class RegistrationResponse {
    private String message;
    private UserDto user;
}
