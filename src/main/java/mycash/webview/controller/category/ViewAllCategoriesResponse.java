package mycash.webview.controller.category;

import lombok.Data;
import lombok.experimental.Accessors;
import mycash.service.dto.CategoryDto;

import java.util.List;

@Data
@Accessors(chain = true)
public class ViewAllCategoriesResponse {
    private String message;
    private List<CategoryDto> categories;
}
