package mycash.webview.controller.category;

import mycash.webview.controller.Controller;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import mycash.service.dto.CategoryDto;
import mycash.service.modelservices.CategoryService;

import java.util.List;

@Service(value = "/authed/view-transac-categories")
public class ViewAllCategoriesController extends Controller<ViewAllCategoriesRequest, ViewAllCategoriesResponse> {
    public ViewAllCategoriesController() {
        super.setHttpMethodName("GET");
    }

    @Override
    public ViewAllCategoriesResponse handle(ApplicationContext context) {
        ViewAllCategoriesResponse response = new ViewAllCategoriesResponse();
        List<CategoryDto> categories = context.getBean(CategoryService.class).viewAllCategories();
        response.setMessage("Transaction categories")
                .setCategories(categories);
        return response;
    }

    @Override
    public Class<ViewAllCategoriesRequest> getRequestClass() {
        return ViewAllCategoriesRequest.class;
    }
}
