package mycash.webview.controller.account;

import lombok.Data;
import mycash.service.dto.AccountDto;

@Data
public class CreateAccountResponse {
    private String message;
    private AccountDto account;
}
