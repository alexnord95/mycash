package mycash.webview.controller.account;

import mycash.webview.controller.Controller;
import mycash.webview.servlet.AuthUserServlet;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import mycash.service.dto.AccountDto;
import mycash.service.dto.UserDto;
import mycash.service.modelservices.AccountService;

import java.util.Collections;
import java.util.List;

@Service(value = "/authed/view-all-accs")
public class ViewAllAccsController extends Controller<ViewAllAccsRequest, ViewAllAccsResponse> {
    public ViewAllAccsController() {
        super.setHttpMethodName("GET");
    }

    @Override
    public ViewAllAccsResponse handle(ApplicationContext context) {
        ViewAllAccsResponse response = new ViewAllAccsResponse();
        UserDto user = (UserDto) AuthUserServlet.getSession().getAttribute("user");
        List<AccountDto> accounts = context.getBean(AccountService.class).viewAllAccByUserId(
                user.getUserId());
        if (accounts == null | accounts.size() == 0) {
            response.setMessage("Accounts not found")
                    .setAccounts(Collections.EMPTY_LIST);
        } else {
            response.setMessage("User accounts")
                    .setAccounts(accounts);
        }
        return response;
    }

    @Override
    public Class<ViewAllAccsRequest> getRequestClass() {
        return ViewAllAccsRequest.class;
    }
}
