package mycash.webview.controller.account;

import lombok.Data;

@Data
public class CreateAccountRequest {
    private String title;
    private Integer accType;
}
