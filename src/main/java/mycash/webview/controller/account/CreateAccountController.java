package mycash.webview.controller.account;

import mycash.webview.controller.Controller;
import mycash.webview.servlet.AuthUserServlet;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import mycash.service.dto.AccountDto;
import mycash.service.dto.UserDto;
import mycash.service.modelservices.AccountService;

@Service(value = "/authed/new-acc")
public class CreateAccountController extends Controller<CreateAccountRequest, CreateAccountResponse> {
    public CreateAccountController() {
        super.setHttpMethodName("POST");
    }

    @Override
    public CreateAccountResponse handle(CreateAccountRequest request, ApplicationContext context) {
        CreateAccountResponse response = new CreateAccountResponse();
        UserDto user = (UserDto) AuthUserServlet.getSession().getAttribute("user");
        AccountDto account = context.getBean(AccountService.class).registerNewAccount(
                user.getUserId(),
                request.getTitle(),
                request.getAccType());
        if (account != null) {
            response.setMessage("Account created");
            response.setAccount(account);
        } else {
            response.setMessage("Account creation failed");
            response.setAccount(new AccountDto());
        }
        return response;
    }

    @Override
    public Class<CreateAccountRequest> getRequestClass() {
        return CreateAccountRequest.class;
    }
}
