package mycash.webview.controller.account;

import lombok.Data;
import lombok.experimental.Accessors;
import mycash.service.dto.AccountDto;

import java.util.List;

@Data
@Accessors(chain = true)
public class ViewAllAccsResponse {
    private String message;
    private List<AccountDto> accounts;
}
