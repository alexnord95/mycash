package mycash.webview.controller;

import lombok.Data;
import org.springframework.context.ApplicationContext;

@Data
public class Controller<REQ, RESP> {

    private String httpMethodName;

    public RESP handle(REQ req, ApplicationContext context) {
        return null;
    }

    public RESP handle(ApplicationContext context) {
        return null;
    }

    public Class<REQ> getRequestClass() {
        return null;
    }
}
