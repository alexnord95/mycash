package mycash.webview.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import mycash.webview.controller.Controller;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import mycash.webview.WebViewConfiguration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NotAuthUserServlet extends HttpServlet {
    private final ObjectMapper mapper = new ObjectMapper();
    private ApplicationContext context = new AnnotationConfigApplicationContext(WebViewConfiguration.class);
    private Controller controller;


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AuthUserServlet.setSession(req.getSession());
        controller = (Controller) context.getBean(req.getRequestURI());
        if (controller == null) {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        if (!(controller.getHttpMethodName().equals(req.getMethod()))) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        Object request;
        try {
            request = mapper.readValue(req.getInputStream(), controller.getRequestClass());
        } catch (IOException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        resp.setHeader("Content-Type", "application/json");
        mapper.writeValue(resp.getWriter(), controller.handle(request, context));
    }
}
