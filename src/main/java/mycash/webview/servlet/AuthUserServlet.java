package mycash.webview.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import mycash.webview.controller.Controller;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import mycash.webview.WebViewConfiguration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthUserServlet extends HttpServlet {
    @Getter
    @Setter
    private static HttpSession session;
    private final ObjectMapper mapper = new ObjectMapper();
    private final ApplicationContext context = new AnnotationConfigApplicationContext(WebViewConfiguration.class);
    private Controller controller;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (session == null) {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        controller = (Controller) context.getBean(req.getRequestURI());
        if (controller == null) {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        if (!(controller.getHttpMethodName().equals(req.getMethod()))) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        resp.setHeader("Content-Type", "application/json");
        mapper.writeValue(resp.getWriter(), controller.handle(context));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (session == null) {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        controller = (Controller) context.getBean(req.getRequestURI());
        if (controller == null) {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        if (!(controller.getHttpMethodName().equals(req.getMethod()))) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        Object request;
        try {
            request = mapper.readValue(req.getInputStream(), controller.getRequestClass());
        } catch (IOException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        resp.setHeader("Content-Type", "application/json");
        mapper.writeValue(resp.getWriter(), controller.handle(request, context));
    }
}
