package mycash.webview;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import mycash.service.ServiceConfiguration;

@Configuration
@ComponentScan
@Import(ServiceConfiguration.class)
public class WebViewConfiguration {
}
