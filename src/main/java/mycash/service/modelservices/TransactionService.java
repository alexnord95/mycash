package mycash.service.modelservices;

import lombok.RequiredArgsConstructor;
import mycash.db.dao.AccountDao;
import mycash.db.dao.TransactionDao;
import mycash.db.dao.UserDao;
import mycash.db.entities.Account;
import mycash.db.entities.Category;
import mycash.db.entities.Transaction;
import mycash.db.entities.User;
import mycash.service.convert.ConvertService;
import mycash.service.create.CreateService;
import mycash.service.dto.TransactionDto;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service

public class TransactionService {
    private final TransactionDao transactionDao;
    private final AccountDao accountDao;
    private final UserDao userDao;
    private final ConvertService convertService;
    private final AccountService accountService;
    private final CategoryService categoryService;
    private final CreateService createService;


    public List<TransactionDto> viewAllTransactions(Integer accNum, Integer userId) {
        List<TransactionDto> transactionDtos = new ArrayList<>();
        User user = userDao.findById(userId);
        Account account = accountDao.findByNumberAndUser(accNum, user);
        if (account != null) {
            List<Transaction> transactions = transactionDao.findByAcc(account);
            if (transactions.size() != 0) {
                for (Transaction transaction : transactions) {
                    transactionDtos.add(convertService.convert(transaction));
                }
            }
        }
        return transactionDtos;
    }

    public TransactionDto registerNewTransaction(Integer accSourceNum, Integer accDestNum, String operation, BigDecimal amount, String categoryId, Integer userId) {
        User user = userDao.findById(userId);
        Account accSource = accountDao.findByNumberAndUser(accSourceNum, user);
        Account accDestination = accountDao.findByNumber(accDestNum);
        List<Category> categories = categoryService.findCategoriesById(categoryId);
        TransactionDto transactionDto = null;
        if (accSource != null && accDestination != null) {
            Transaction transaction = createService.createNewTransaction(accSource, accDestination, operation, amount, categories);
            if (transaction != null) {
                transactionDto = convertService.convert(transaction);
                accountService.updateForTransfer(accSource, accDestination, amount);
            }
        }
        return transactionDto;
    }
}
