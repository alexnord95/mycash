package mycash.service.modelservices;

import mycash.db.dao.UserDao;
import mycash.db.entities.User;
import lombok.RequiredArgsConstructor;
import mycash.service.hash.HashService;
import org.springframework.stereotype.Service;
import mycash.service.convert.ConvertService;
import mycash.service.create.CreateService;
import mycash.service.dto.UserDto;

@RequiredArgsConstructor
@Service
public class UserService {
    private final UserDao userDao;
    private final HashService hashService;
    private final ConvertService convertService;
    private final CreateService createService;

    public UserDto auth(String name, String pass) {
        User user = userDao.findByNameAndPass(name, hashService.hash(pass));
        UserDto userDto = null;
        if (user != null) {
            userDto = convertService.convert(user);
        }
        return userDto;
    }

    public UserDto registerNewUser(String name, String email, String password) {
        UserDto userDto = null;
        User user = createService.createNewUser(name, email, password);
        if (user != null) {
            userDto = convertService.convert(user);
        }
        return userDto;
    }
}
