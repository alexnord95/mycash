package mycash.service.modelservices;

import mycash.db.dao.TypeDao;
import mycash.db.entities.Type;
import lombok.RequiredArgsConstructor;
import mycash.service.convert.ConvertService;
import org.springframework.stereotype.Service;
import mycash.service.dto.TypeDto;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class TypeService {
    private final TypeDao typeDao;
    private final ConvertService convertService;

    public List<TypeDto> viewAllTypes() {
        List<TypeDto> typeDtos = new ArrayList<>();
        List<Type> types = typeDao.findByAll();
        if (types.size() != 0) {
            for (Type type : types) {
                typeDtos.add(convertService.convert(type));
            }
        }
        return typeDtos;
    }
}
