package mycash.service.modelservices;

import mycash.db.dao.AccountDao;
import mycash.db.dao.TypeDao;
import mycash.db.dao.UserDao;
import mycash.db.entities.Account;
import mycash.db.entities.Type;
import mycash.db.entities.User;
import lombok.RequiredArgsConstructor;
import mycash.service.convert.ConvertService;
import org.springframework.stereotype.Service;
import mycash.service.create.CreateService;
import mycash.service.dto.AccountDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class AccountService {
    private final UserDao userDao;
    private final AccountDao accountDao;
    private final TypeDao typeDao;
    private final ConvertService convertService;
    private final CreateService createService;

    public List<AccountDto> viewAllAccByUserId(Integer id) {
        User user = userDao.findById(id);
        List<AccountDto> accountDtos = new ArrayList<>();
        if (user != null) {
            List<Account> accounts = accountDao.findByUser(user);
            if (accounts.size() != 0) {
                for (Account account : accounts) {
                    accountDtos.add(convertService.convert(account));
                }
            }
        }
        return accountDtos;
    }

    public void updateForTransfer(Account accSource, Account accDestination, BigDecimal amount) {
        accSource.setBalance(accSource.getBalance().subtract(amount));
        accDestination.setBalance(accDestination.getBalance().add(amount));
        accountDao.update(accSource);
        accountDao.update(accDestination);
    }

    public AccountDto registerNewAccount(Integer userId, String title, Integer accTypeId) {
        AccountDto accountDto = null;
        User user = userDao.findById(userId);
        Type type = typeDao.findById(accTypeId);
        if (user != null) {
            if (type != null) {
                Account account = createService.createNewAccount(user, title, type);
                if (account != null) {
                    accountDto = convertService.convert(account);
                }
            }
        }
        return accountDto;
    }


}
