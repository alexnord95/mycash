package mycash.service.modelservices;

import lombok.RequiredArgsConstructor;
import mycash.db.dao.CategoryDao;
import mycash.db.entities.Category;
import mycash.service.convert.ConvertService;
import mycash.service.create.CreateService;
import mycash.service.dto.CategoryDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CategoryService {
    private final CategoryDao categoryDao;
    private final ConvertService convertService;
    private final CreateService createService;

    public List<CategoryDto> viewAllCategories() {
        List<CategoryDto> categoryDtos = new ArrayList<>();
        List<Category> categories = categoryDao.findByAll();
        if (categories.size() != 0) {
            for (Category category : categories) {
                categoryDtos.add(convertService.convert(category));
            }
        }
        return categoryDtos;
    }

    public List<Category> findCategoriesById(String categories) {
        ArrayList<Category> categoryDtos = new ArrayList<>();
        for (String cat : categories.split(" ")) {
            Category category = categoryDao.findById(Integer.valueOf(cat));
            if (category != null) {
                categoryDtos.add(category);
            }
        }
        return categoryDtos;
    }

    public CategoryDto registerNewCategory(String name) {
        CategoryDto categoryDto = null;
        Category category = createService.createNewCategory(name);
        if (category != null) {
            categoryDto = convertService.convert(category);
        }
        return categoryDto;
    }
}
