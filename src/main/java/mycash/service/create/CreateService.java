package mycash.service.create;

import lombok.RequiredArgsConstructor;
import mycash.db.dao.AccountDao;
import mycash.db.dao.CategoryDao;
import mycash.db.dao.TransactionDao;
import mycash.db.dao.UserDao;
import mycash.db.entities.*;
import mycash.service.dto.CategoryDto;
import mycash.service.hash.HashService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CreateService {
    private final UserDao userDao;
    private final AccountDao accountDao;
    private final TransactionDao transactionDao;
    private final CategoryDao categoryDao;
    private final HashService hashService;

    public User createNewUser(String name, String email, String password) {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(hashService.hash(password));
        user.setActive(true);
        return userDao.insert(user);
    }

    public Account createNewAccount(User user, String title, Type accType) {
        List<Account> userAccounts = accountDao.findByUser(user);
        if (userAccounts.size() == 5) {
            System.out.println("Превышен лимит на создание счетов");
            return null;
        }
        Account account = new Account();
        account.setUser(user);
        account.setTitle(title);
        account.setDate(getNewDate());
        account.setBalance(BigDecimal.ZERO);
        account.setType(accType);
        return accountDao.insert(account);
    }

    public Transaction createNewTransaction(Account accSource, Account accDest, String operation, BigDecimal amount, List<Category> categories) {
        Transaction transaction = new Transaction();
        transaction.setAccountSource(accSource);
        transaction.setAccountDestination(accDest);
        if (operation.isEmpty()) {
            transaction.setOperation("Перевод средств");
        } else {
            transaction.setOperation(operation);
        }
        transaction.setAmount(amount);
        transaction.setDate(getNewDate());
        transaction.setCategories(categories);
        return transactionDao.insert(transaction);
    }

    public Category createNewCategory(String name) {
        Category category = new Category();
        category.setName(name);
        return categoryDao.insert(category);
    }

    private Date getNewDate() {
        return new Date(Calendar.getInstance().getTimeInMillis());
    }

}

