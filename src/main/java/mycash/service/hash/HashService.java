package mycash.service.hash;

public interface HashService {
    String hash(String pass);
}
