package mycash.service.hash;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

@Service
public class Sha512HashService implements HashService {
    @Override
    public String hash(String pass) {
        return DigestUtils.sha512Hex(pass);
    }
}
