package mycash.service;

import mycash.db.dao.JpaConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan
@Import(JpaConfiguration.class)
public class ServiceConfiguration {
}
