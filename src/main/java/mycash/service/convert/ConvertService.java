package mycash.service.convert;

import mycash.db.entities.*;
import mycash.service.dto.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConvertService {

    public UserDto convert(User user) {
        UserDto convUser = new UserDto();
        convUser
                .setUserId(user.getUserId())
                .setName(user.getName())
                .setEmail(user.getEmail())
                .setActive(user.isActive());

        return convUser;
    }

    public AccountDto convert(Account account) {
        AccountDto convAccount = new AccountDto();
        convAccount
                .setAccountId(account.getAccountId())
                .setUser(convert(account.getUser()))
                .setTitle(account.getTitle())
                .setDate(account.getDate())
                .setBalance(account.getBalance())
                .setType(convert(account.getType()));

        return convAccount;
    }

    public TransactionDto convert(Transaction transaction) {
        TransactionDto convTransaction = new TransactionDto();
        List<CategoryDto> convCategories = new ArrayList<>();
        List<Category> categories = transaction.getCategories();
        for (Category category : categories) {
            convCategories.add(convert(category));
        }
        convTransaction
                .setTransactionId(transaction.getTransactionId())
                .setAccountSource(convert(transaction.getAccountSource()))
                .setAccountDestination(convert(transaction.getAccountDestination()))
                .setCategories(convCategories)
                .setOperation(transaction.getOperation())
                .setAmount(transaction.getAmount())
                .setDate(transaction.getDate());

        return convTransaction;
    }

    public TypeDto convert(Type type) {
        TypeDto convType = new TypeDto();
        convType
                .setTypeId(type.getTypeId())
                .setName(type.getName());

        return convType;
    }

    public CategoryDto convert(Category category) {
        CategoryDto convCategory = new CategoryDto();
        convCategory
                .setCategoryId(category.getCategoryId())
                .setName(category.getName());

        return convCategory;
    }
}
