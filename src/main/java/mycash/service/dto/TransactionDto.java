package mycash.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import mycash.db.entities.Category;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

@Data
@EqualsAndHashCode(of = {"transactionId", "accountSource", "accountDestination"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TransactionDto {
    private int transactionId;
    private AccountDto accountSource;
    private AccountDto accountDestination;
    private List<CategoryDto> categories;
    private String operation;
    private BigDecimal amount;
    private Date date;

}
