package mycash.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.sql.Date;

@Data
@EqualsAndHashCode(of = {"accountId", "user", "accountNumber"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AccountDto {
    private int accountId;
    private UserDto user;
    private String title;
    private Date date;
    private BigDecimal balance;
    private TypeDto type;
    private int accountNumber;
}
