package mycash.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(of = {"typeId", "name"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TypeDto {
    private int typeId;
    private String name;

}
