package mycash;

import mycash.view.CommandViewer;

import java.lang.reflect.InvocationTargetException;

public class MyCash {
    public static void main(String[] args) {
        try {
            new CommandViewer().greeting();
        } catch (InvocationTargetException | IllegalAccessException e) {
            System.out.println(e.getCause());
        }
    }

}

