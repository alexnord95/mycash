package mycash.view;

import mycash.service.dto.*;
import mycash.service.modelservices.*;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static mycash.view.CommandViewer.*;

@Service
public class CommandListener {
    private static int count = 0;
    private final String delimeter = " | ";
    private final StringBuilder builder = new StringBuilder();


    @AuthCommand(number = "0", description = "0.Выход из учетной записи")
    public void notAuthCloseApp() throws InvocationTargetException, IllegalAccessException {
        CommandViewer.setUser(null);
        viewNotAuthCommands();

    }

    @AuthCommand(number = "1", description = "1.Вывод списка счетов")
    public void viewAccounts(ApplicationContext context, Integer userId) {
        List<AccountDto> accountDtos = context.getBean(AccountService.class).viewAllAccByUserId(userId);
        if (accountDtos.size() == 0) {
            System.out.println("У вас ещё нет счетов");
        } else {
            for (AccountDto accountDto : accountDtos) {
                System.out.println(builder
                        .append("Номер счёта: ").append(accountDto.getAccountNumber()).append(delimeter)
                        .append("Наименование счёта: ").append(accountDto.getTitle()).append(delimeter)
                        .append("Дата создания: ").append(accountDto.getDate()).append(delimeter)
                        .append("Баланс: ").append(accountDto.getBalance()).append(delimeter)
                        .append("Тип счёта: ").append(accountDto.getType().getName()));
                builder.setLength(0);
            }
        }
    }

    @AuthCommand(number = "2", description = "2.Вывод списка транзакций по счёту")
    public void viewAccTransactions(ApplicationContext context, Integer userId) {
        List<TransactionDto> transactions = context.getBean(TransactionService.class).viewAllTransactions(
                requestIntData("Введите номер вашего счёта: "),
                userId);
        if (transactions.size() == 0) {
            System.out.println("По выбранному счёту нет транзакций");
        } else {
            for (TransactionDto transaction : transactions) {
                showTransactionInfo(transaction);
            }
        }
    }

    @AuthCommand(number = "3", description = "3.Вывод списка типов счетов")
    public void viewAccTypes(ApplicationContext context) {
        List<TypeDto> typeDtos = context.getBean(TypeService.class).viewAllTypes();
        for (TypeDto type : typeDtos) {
            System.out.println(builder
                    .append("ID типа счёта: ").append(type.getTypeId()).append(delimeter)
                    .append("Наименование типа счёта: ").append(type.getName()));
            builder.setLength(0);
        }
    }

    @AuthCommand(number = "4", description = "4.Вывод списка категорий транзакций")
    public void viewTransactionCategories(ApplicationContext context) {
        List<CategoryDto> categoryDtos = context.getBean(CategoryService.class).viewAllCategories();
        for (CategoryDto category : categoryDtos) {
            System.out.println(builder
                    .append("ID категории транзакции: ").append(category.getCategoryId()).append(delimeter)
                    .append("Наименование категории транзакции: ").append(category.getName()));
            builder.setLength(0);
        }
    }

    @AuthCommand(number = "5", description = "5.Создание нового счёта")
    public void createNewAccount(ApplicationContext context, Integer userId) {
        AccountDto account = context.getBean(AccountService.class).registerNewAccount(
                userId,
                requestStringData("Введите наименование счёта: "),
                requestIntData("Введите номер типа счёта: ")
        );

        if (account != null) {
            System.out.println("Новый счёт добавлен");
            System.out.println(builder
                    .append("Наименование счёта: ").append(account.getTitle()).append(delimeter)
                    .append("Дата создания: ").append(account.getDate()).append(delimeter)
                    .append("Баланс: ").append(account.getBalance()).append(delimeter)
                    .append("Тип счёта: ").append(account.getType().getName()));
            builder.setLength(0);
        }
    }

    @AuthCommand(number = "6", description = "6.Перевод средств на другой счёт")
    public void transferMoney(ApplicationContext context, Integer userId) {
        TransactionDto transactionDto = context.getBean(TransactionService.class).registerNewTransaction(
                requestIntData("Введите номер вашего счёта: "),
                requestIntData("Введите номер счёта получателя: "),
                requestStringData("Введите описание операции: "),
                requestDigDecimalData("Введите сумму операции: "),
                requestStringData("Введите номер категории транзакции через пробел: "),
                userId);
        if (transactionDto != null) {
            System.out.println("Добавлена новая транзакция");
            showTransactionInfo(transactionDto);
        } else {
            System.out.println("Транзакция не удалась");
        }
    }

    private void showTransactionInfo(TransactionDto transactionDto) {
        StringBuilder strb = new StringBuilder();
        for (CategoryDto category : transactionDto.getCategories()) {
            strb.append(category.getName()).append(" ");
        }
        System.out.println(builder
                .append("Номер счёта отправителя: ").append(transactionDto.getAccountSource().getAccountNumber()).append(delimeter)
                .append("Номер счёта получателя: ").append(transactionDto.getAccountDestination().getAccountNumber()).append(delimeter)
                .append("Описание операции: ").append(transactionDto.getOperation()).append(delimeter)
                .append("Сумма: ").append(transactionDto.getAmount()).append(delimeter)
                .append("Дата проведения транзакции: ").append(transactionDto.getDate()).append(delimeter)
                .append("Категории транзакции: ").append(strb.toString()));
        builder.setLength(0);
    }


    @NotAuthCommand(number = "0", description = "0.Выход из приложения")
    public void closeApp() {
        System.exit(2);
    }

    @NotAuthCommand(number = "1", description = "1.Регистрация нового пользователя")
    public void userRegistration(ApplicationContext context) {
        String name = requestStringData("Введите имя пользователя: ");
        String email = requestStringData("Введите email: ");
        String pass = requestStringData("Введите пароль: ");
        UserDto user = context.getBean(UserService.class).registerNewUser(name.toLowerCase(), email.toLowerCase(), pass);
        if (user != null) {
            System.out.println("Пользователь зарегистрирован");
            System.out.println(builder
                    .append("Имя пользователя: ").append(user.getName()).append(delimeter)
                    .append("Email: ").append(user.getEmail()));
            builder.setLength(0);
        }
    }

    @NotAuthCommand(number = "2", description = "2.Авторизация пользователя")
    public void userAuth(ApplicationContext context) throws InvocationTargetException, IllegalAccessException {
        String name = requestStringData("Введите логин: ");
        String pass = requestStringData("Введите пароль: ");
        UserDto userDto = context.getBean(UserService.class).auth(name.toLowerCase(), pass);
        if (userDto != null) {
            CommandViewer.setUser(userDto);
            viewAuthCommands();
        } else if (count < 3) {
            System.out.println("Введён неправильный логин или пароль");
            count++;
            viewNotAuthCommands();
        } else {
            System.out.println("Превышено количество попыток авторизации");
            System.exit(1);
        }
    }


    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface AuthCommand {
        String number();

        String description();
    }

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface NotAuthCommand {
        String number();

        String description();
    }
}
