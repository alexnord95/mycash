package mycash.view;

import mycash.service.dto.UserDto;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

@Service
public class CommandViewer {
    private static final HashMap<String, Method> authCommands = new HashMap<>();
    private static final HashMap<String, Method> notAuthCommands = new HashMap<>();
    private static final List<String> authCommDescription = new ArrayList<>();
    private static final List<String> notAuthCommDescription = new ArrayList<>();
    private static final CommandListener commListener = new CommandListener();
    private static UserDto user;
    private static ApplicationContext context;

    static {
        for (Method method : commListener.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(CommandListener.AuthCommand.class)) {
                CommandListener.AuthCommand authCommand = method.getAnnotation(CommandListener.AuthCommand.class);
                authCommands.put(authCommand.number(), method);
                authCommDescription.add(authCommand.description());
            } else if (method.isAnnotationPresent(CommandListener.NotAuthCommand.class)) {
                CommandListener.NotAuthCommand notAuthCommand = method.getAnnotation(CommandListener.NotAuthCommand.class);
                notAuthCommands.put(notAuthCommand.number(), method);
                notAuthCommDescription.add(notAuthCommand.description());

            }
        }
        Collections.sort(authCommDescription);
        Collections.sort(notAuthCommDescription);
    }

    static void viewNotAuthCommands() throws InvocationTargetException, IllegalAccessException {
        for (String decsription : notAuthCommDescription) {
            System.out.println(decsription);
        }
        callCommands(requestStringData("Введите номер команды из списка: "));

    }

    static void viewAuthCommands() throws InvocationTargetException, IllegalAccessException {
        for (String decsription : authCommDescription) {
            System.out.println(decsription);
        }
        callCommands(requestStringData("Введите номер команды из списка: "));
    }

    static void callCommands(String id) throws InvocationTargetException, IllegalAccessException {
        if (user != null) {
            Method method = authCommands.get(id);
            switch (id) {
                case ("0"):
                    method.invoke(commListener);
                    break;
                case ("1"):
                case ("2"):
                case ("5"):
                case ("6"):
                    method.invoke(commListener, context, user.getUserId());
                    pressEnterToContinue();
                    break;
                case ("3"):
                case ("4"):
                    method.invoke(commListener, context);
                    pressEnterToContinue();
                    break;
                default:
                    System.out.println("Введён неправильный номер команды");
                    pressEnterToContinue();
                    break;
            }
        } else {
            Method method = notAuthCommands.get(id);
            switch (id) {
                case ("0"):
                    method.invoke(commListener);
                    break;
                case ("1"):
                case ("2"):
                    method.invoke(commListener, context);
                    pressEnterToContinue();
                    break;
                default:
                    System.out.println("Введён неправильный номер команды");
                    pressEnterToContinue();

                    break;
            }
        }
    }

    private static void pressEnterToContinue() throws InvocationTargetException, IllegalAccessException {
        System.out.println("\nНажмите клавишу Enter для продолжения...");
        try {
            System.in.read();
            if (user != null) {
                viewAuthCommands();
            } else {
                viewNotAuthCommands();
            }
        } catch (IOException throwables) {
            throw new RuntimeException(throwables.getMessage(), throwables);
        }
    }

    public static void setUser(UserDto user) {
        CommandViewer.user = user;
    }

    static String requestStringData(String message) {
        Scanner in = new Scanner(System.in);
        System.out.println(message);
        return in.nextLine();
    }

    static Integer requestIntData(String message) {
        Scanner in = new Scanner(System.in);
        System.out.println(message);
        return in.nextInt();
    }

    static BigDecimal requestDigDecimalData(String message) {
        Scanner in = new Scanner(System.in);
        System.out.println(message);
        return in.nextBigDecimal();
    }

    public void greeting() throws InvocationTargetException, IllegalAccessException {
        context = new AnnotationConfigApplicationContext(ViewConfiguration.class);
        System.out.println("Здравствуйте гость, пожалуйста выберите номер команды из списка: ");
        viewNotAuthCommands();
    }
}
