package mycash.db.entities;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@EqualsAndHashCode(of = {"userId", "name", "password"})
@ToString(exclude = {"password"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(
                name = "User.findByNameAndPass",
                query = "SELECT us FROM User us WHERE us.name =: name AND us.password =: password"),
        @NamedQuery(
                name = "User.findById",
                query = "SELECT us FROM User us WHERE us.userId =: id"),
        @NamedQuery(
                name = "User.findByAll",
                query = "SELECT us FROM User us "),
        @NamedQuery(
                name = "User.delete",
                query = "DELETE FROM User us WHERE us.userId =: id")
})
public class User {
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = IDENTITY)
    private int userId;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "is_active")
    private boolean isActive;
}
