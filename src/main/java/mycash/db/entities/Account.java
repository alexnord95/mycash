package mycash.db.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@EqualsAndHashCode(of = {"accountId", "accountNumber"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "accounts")
@NamedQueries({
        @NamedQuery(name = "Account.findByNumber", query = "SELECT acc FROM Account acc WHERE acc.accountNumber =: number"),
        @NamedQuery(name = "Account.findByNumberAndUser", query = "SELECT acc FROM Account acc WHERE acc.accountNumber =: number AND acc.user =: user"),
        @NamedQuery(name = "Account.findByUser", query = "SELECT acc FROM Account acc WHERE acc.user =: user"),
        @NamedQuery(name = "Account.findById", query = "SELECT acc FROM Account acc WHERE acc.accountId =: id"),
        @NamedQuery(name = "Account.findByAll", query = "SELECT acc FROM Account acc "),
        @NamedQuery(name = "Account.delete", query = "DELETE FROM Account acc WHERE acc.accountId =: id")
})
public class Account {
    @Id
    @Column(name = "account_id")
    @GeneratedValue(strategy = IDENTITY)
    private int accountId;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "title")
    private String title;

    @Column(name = "date")
    private Date date;

    @Column(name = "balance")
    private BigDecimal balance;

    @OneToOne(fetch = LAZY)
    @JoinColumn(name = "type")
    private Type type;

    @Column(name = "account_number")
    @Generated(value = GenerationTime.INSERT)
    private int accountNumber;

}


