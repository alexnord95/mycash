package mycash.db.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@EqualsAndHashCode(of = {"transactionId", "amount"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "transactions")
@NamedQueries({
        @NamedQuery(name = "Transaction.findByAcc", query = "SELECT trsc FROM Transaction trsc WHERE trsc.accountSource =: account"),
        @NamedQuery(name = "Transaction.findById", query = "SELECT trsc FROM Transaction trsc WHERE trsc.transactionId =: id"),
        @NamedQuery(name = "Transaction.findByAll", query = "SELECT trsc FROM Transaction trsc "),
        @NamedQuery(name = "Transaction.delete", query = "DELETE FROM Transaction trsc WHERE trsc.transactionId =: id")
})
public class Transaction {
    @Id
    @Column(name = "transaction_id")
    @GeneratedValue(strategy = IDENTITY)
    private int transactionId;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "account_source_num")
    private Account accountSource;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "account_destination_num")
    private Account accountDestination;

    @ManyToMany(fetch = LAZY)
    @JoinTable(
            name = "category_to_transaction",
            joinColumns = @JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "category_id"))
    private List<Category> categories;

    @Column(name = "operation")
    private String operation;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "date")
    private Date date;
}
