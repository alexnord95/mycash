package mycash.db.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@EqualsAndHashCode(of = {"categoryId", "name"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "categories")
@NamedQueries({
        @NamedQuery(name = "Category.findById", query = "SELECT cat FROM Category cat WHERE cat.categoryId =: id"),
        @NamedQuery(name = "Category.findByAll", query = "SELECT cat FROM Category cat "),
        @NamedQuery(name = "Category.delete", query = "DELETE FROM Category cat WHERE cat.categoryId =: id")
})
public class Category {
    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy = IDENTITY)
    private int categoryId;

    @Column(name = "name")
    private String name;
}
