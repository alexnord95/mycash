package mycash.db.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@EqualsAndHashCode(of = {"typeId", "name"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "types")
@NamedQueries({
        @NamedQuery(name = "Type.findById", query = "SELECT ty FROM Type ty WHERE ty.typeId =: id"),
        @NamedQuery(name = "Type.findByAll", query = "SELECT ty FROM Type ty "),
        @NamedQuery(name = "Type.delete", query = "DELETE FROM Type ty WHERE ty.typeId =: id")
})
public class Type {
    @Id
    @Column(name = "type_id")
    @GeneratedValue(strategy = IDENTITY)
    private int typeId;

    @Column(name = "name")
    private String name;
}
