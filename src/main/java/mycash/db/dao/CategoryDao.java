package mycash.db.dao;

import mycash.db.entities.Category;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class CategoryDao extends AbstractDao<Category, Integer> {
    public CategoryDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected String findByIdSQL() {
        return "Category.findById";
    }

    @Override
    protected String findByAllSQL() {
        return "Category.findByAll";
    }

    @Override
    protected String deleteSQL() {
        return "Category.delete";
    }

    @Override
    public Class<Category> getDomainClass() {
        return Category.class;
    }
}
