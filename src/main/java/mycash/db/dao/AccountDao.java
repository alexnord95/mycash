package mycash.db.dao;

import mycash.db.entities.Account;
import mycash.db.entities.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Collections;
import java.util.List;

@Service
public class AccountDao extends AbstractDao<Account, Integer> {
    public AccountDao(EntityManager entityManager) {
        super(entityManager);
    }

    public Account findByNumberAndUser(Integer accNum, User user) {
        Account account;
        try {
            account = entityManager
                    .createNamedQuery("Account.findByNumberAndUser", getDomainClass())
                    .setParameter("number", accNum)
                    .setParameter("user", user)
                    .getSingleResult();
        } catch (NoResultException e) {
            account = null;
        }
        return account;
    }

    public Account findByNumber(Integer accNum) {
        Account account;
        try {
            account = entityManager
                    .createNamedQuery("Account.findByNumber", getDomainClass())
                    .setParameter("number", accNum)
                    .getSingleResult();
        } catch (NoResultException e) {
            account = null;
        }
        return account;
    }

    public List<Account> findByUser(User user) {
        List<Account> accounts = entityManager
                .createNamedQuery("Account.findByUser", getDomainClass())
                .setParameter("user", user)
                .getResultList();
        if (accounts == null | accounts.size() == 0) {
            accounts = Collections.EMPTY_LIST;
        }
        return accounts;
    }

    @Override
    protected String findByIdSQL() {
        return "Account.findById";
    }

    @Override
    protected String findByAllSQL() {
        return "Account.findByAll";
    }

    @Override
    protected String deleteSQL() {
        return "Account.delete";
    }

    @Override
    public Class<Account> getDomainClass() {
        return Account.class;
    }
}
