package mycash.db.dao;

import mycash.db.entities.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

@Service
public class UserDao extends AbstractDao<User, Integer> {
    public UserDao( EntityManager entityManager) {
        super(entityManager);
    }

    public User findByNameAndPass(String name, String pass) {
        User user;
        try {
            user = entityManager
                    .createNamedQuery("User.findByNameAndPass", getDomainClass())
                    .setParameter("name", name)
                    .setParameter("password", pass)
                    .getSingleResult();
        } catch (NoResultException e) {
            user = null;
        }
        return user;
    }

    @Override
    protected String findByIdSQL() {
        return "User.findById";
    }

    @Override
    protected String findByAllSQL() {
        return "User.findByAll";
    }

    @Override
    protected String deleteSQL() {
        return "User.delete";
    }

    @Override
    public Class<User> getDomainClass() {
        return User.class;
    }
}
