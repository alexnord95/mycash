package mycash.db.dao;

import mycash.db.entities.Account;
import mycash.db.entities.Transaction;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Service
public class TransactionDao extends AbstractDao<Transaction, Integer> {
    public TransactionDao(EntityManager entityManager) {
        super(entityManager);
    }

    public List<Transaction> findByAcc(Account acc) {
        List<Transaction> transactions = entityManager
                .createNamedQuery("Transaction.findByAcc", getDomainClass())
                .setParameter("account", acc)
                .getResultList();
        if (transactions == null | transactions.size() == 0) {
            transactions = Collections.EMPTY_LIST;
        }
        return transactions;
    }

    @Override
    protected String findByIdSQL() {
        return "Transaction.findById";
    }

    @Override
    protected String findByAllSQL() {
        return "Transaction.findByAll";
    }

    @Override
    protected String deleteSQL() {
        return "Transaction.delete";
    }

    @Override
    public Class<Transaction> getDomainClass() {
        return Transaction.class;
    }
}
