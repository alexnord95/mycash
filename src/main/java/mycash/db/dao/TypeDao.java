package mycash.db.dao;

import mycash.db.entities.Type;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class TypeDao extends AbstractDao<Type, Integer> {
    public TypeDao( EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected String findByIdSQL() {
        return "Type.findById";
    }

    @Override
    protected String findByAllSQL() {
        return "Type.findByAll";
    }

    @Override
    protected String deleteSQL() {
        return "Type.delete";
    }

    @Override
    public Class<Type> getDomainClass() {
        return Type.class;
    }
}
