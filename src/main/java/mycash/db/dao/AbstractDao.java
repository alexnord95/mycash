package mycash.db.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Service
public abstract class AbstractDao<DOMAIN, ID> implements Dao<DOMAIN, ID> {
    protected final EntityManager entityManager;

    protected abstract String findByIdSQL();

    protected abstract String findByAllSQL();

    protected abstract String deleteSQL();

    public abstract Class<DOMAIN> getDomainClass();

    @Override
    public DOMAIN findById(ID id) {
        return entityManager
                .createNamedQuery(findByIdSQL(), getDomainClass())
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<DOMAIN> findByAll() {
        List<DOMAIN> domains = entityManager
                .createNamedQuery(findByAllSQL(), getDomainClass())
                .getResultList();
        if (domains == null | domains.size() == 0) {
            domains = Collections.EMPTY_LIST;
        }
        return domains;
    }

    @Override
    public DOMAIN insert(DOMAIN domain) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(domain);
        transaction.commit();
        return domain;
    }

    @Override
    public DOMAIN update(DOMAIN domain) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(domain);
        transaction.commit();
        return domain;
    }

    @Override
    public boolean delete(ID id) {
        TypedQuery<DOMAIN> query = entityManager
                .createNamedQuery(deleteSQL(), getDomainClass())
                .setParameter("id", id);
        int rows = query.executeUpdate();
        return rows > 0;
    }
}
